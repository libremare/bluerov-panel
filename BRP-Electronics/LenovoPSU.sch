EESchema Schematic File Version 4
LIBS:BRPanel-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4550 2850 0    50   Input ~ 0
AC_IN1
Text HLabel 4500 3250 0    50   Input ~ 0
AC_IN2
Text HLabel 5300 2850 0    50   Input ~ 0
20V
Text HLabel 5300 3300 0    50   Input ~ 0
GND
$EndSCHEMATC
