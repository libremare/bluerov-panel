EESchema Schematic File Version 4
LIBS:BRPanel-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 4800 3550 0    50   Input ~ 0
DCIN+
Text HLabel 4800 3850 0    50   Input ~ 0
DCIN-
Text HLabel 5800 3550 0    50   Input ~ 0
DCOUT+
Text HLabel 5800 3900 0    50   Input ~ 0
DCOUT-
$EndSCHEMATC
